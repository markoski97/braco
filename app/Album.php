<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'album';

    public function images(){
        return $this->hasMany('App\ImageAlbum');
    }
}
