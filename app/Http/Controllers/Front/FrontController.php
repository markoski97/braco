<?php

namespace App\Http\Controllers\Front;

use App\Album;
use App\Category;
use App\Http\Controllers\Controller;
use App\ImageAlbum;
use App\ImagePost;
use App\Mail\ContactMail;
use App\Post;
use App\Slider;
use App\Transport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class FrontController extends Controller
{

    public function index()
    {

        $sliders = Slider::all()->sortByDesc('created_at');
        $posts=Post::orderBy('created_at')->paginate(5);
        return view('front.index.index', compact('sliders','posts'));
    }

    public function uslovi(){

        return view('front.uslovi.index');
    }

    public function arazmani()
    {
        $categories = Category::all();
        return view('front.aranzmani.index', compact('categories'));
    }

    public function show($slug)
    {
        $category = Category::where('slug', '=', $slug)->first();
        return view('front.aranzmani.show')->with('category', $category)
         ->with('post', Post::where('category_id', $category->id)->get());
    }

    public function postojshow($slug)
    {
        $post = Post::where('slug','=', $slug)->first();

        return view('front.postoj.show')->with('post',$post)->with('postimage',ImagePost::where('post_id',$post->id)->get());
    }

    public function prevozindex(){

        $transport=Transport::all();
        return view('front.prevoz.index',compact('transport'));
    }

    public function prevozshow($slug)
    {
        $transport=Transport::where('slug','=',$slug)->first();
        return view('front.prevoz.show',compact('transport'));

    }

    public function albumindex(){
        $album=Album::all();

        return view('front.album.index',compact('album'));
    }

    public function albumshow($slug){
        $album=Album::where('slug','=',$slug)->first();

        return view('front.album.show')->with('album',$album)->with('albumimage',ImageAlbum::where('album_id',$album->id)->get());

    }

    public function contact(){

        return view('front.contact.index');
    }

    public function postcontact(Request $request){

        Mail::to('braco_prilep@yahoo.com')->cc('ex-travel@hotmail.com')->send(new ContactMail($request));
        return redirect('kontakt');
    }
}
