<?php

namespace App\Http\Controllers\admin;

use App\Album;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DemeterChain\A;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AlbumController extends Controller
{

    public function index()
    {
       $album=Album::all();

       return view('admin.album.index',compact('album'));
    }

    public function create()
    {
        return view('admin.album.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif',
        ],
            [
                'title.required' => 'Немате внесено наслов',
                'description.required' => 'Немате внесено опис',
                'image.required' => 'Немате внесено слика'
            ]
        );

        $slug = Str::slug($request->title);

        $image = $request->file('image');

        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '-' . $image->getClientOriginalName();

            if (!file_exists('uploads/album')) {
                mkdir('uploads/album', 0777, true);
            }
            $image->move('uploads/album', $imageName);
        } else {
            $imageName = 'default.png';
        }
        $album = new Album();
        $album->title = $request->title;
        $album->description = $request->description;
        $album->image = $imageName;
        $album->slug = $slug;
        $album->save();
        return redirect()->route('album.index')->with('status', 'Успешно Внесување')->with('status_code', 'success');
    }

    public function edit($id)
    {
        $album = Album::findorfail($id);

        return view('admin.album.edit', compact('album'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ],
            [
                'title.required' => 'Немате внесено наслов',
                'description.required' => 'Немате внесено опис',
                'image.required' => 'Немате внесено слика'
            ]);
        $album = Album::find($id);
        $slug = Str::slug($request->title);

        $image = $request->file('image');
        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '-' . $image->getClientOriginalName();

            if (!file_exists('uploads/album')) {
                mkdir('uploads/album', 0777, true);
            }
            $image->move('uploads/album', $imageName);
        } else {
            $imageName = $album->image;//Ako ne klaj nova slika Zemija starata
        }

        $album->title = $request->title;
        $album->description = $request->description;
        $album->image = $imageName;
        $album->slug = $slug;

        $album->save();

        return redirect()->route('album.index')->with('status', 'Успешна промена')->with('status_code', 'info');
    }

    public function destroy($id)
    {
        $album = Album::find($id);
        if (file_exists('uploads/album/' . $album->image)) {
            unlink('uploads/album/' . $album->image);
        }
        $album->delete();

        return redirect()->back()->with('status', 'Успешно бришење')->with('status_code', 'success');
    }
}
