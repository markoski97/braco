<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();

        return view('admin.category.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.category.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif',
        ],
            [
                'title.required' => 'Немате внесено наслов',
                'description.required' => 'Немате внесено опис',
                'image.required' => 'Немате внесено слика'
            ]
        );

        $slug = Str::slug($request->title);

        $image = $request->file('image');

        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '-' . $image->getClientOriginalName();

            if (!file_exists('uploads/category')) {
                mkdir('uploads/category', 0777, true);
            }
            $image->move('uploads/category', $imageName);
        } else {
            $imageName = 'default.png';
        }
        $category = new Category();
        $category->title = $request->title;
        $category->description = $request->description;
        $category->image = $imageName;
        $category->slug = $slug;
        $category->save();
        return redirect()->route('category.index')->with('status', 'Успешно Внесување')->with('status_code', 'success');
    }

    public function edit($id)
    {
        $categories = Category::findorfail($id);

        return view('admin.category.edit', compact('categories'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ],
            [
                'title.required' => 'Немате внесено наслов',
                'description.required' => 'Немате внесено опис',
                'image.required' => 'Немате внесено слика'
            ]);
        $category = Category::find($id);
        $slug = Str::slug($request->title);

        $image = $request->file('image');
        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '-' . $image->getClientOriginalName();

            if (!file_exists('uploads/category')) {
                mkdir('uploads/category', 0777, true);
            }
            $image->move('uploads/category', $imageName);
        } else {
            $imageName = $category->image;//Ako ne klaj nova slika Zemija starata
        }

        $category->title = $request->title;
        $category->description = $request->description;
        $category->slug = $slug;
        $category->image = $imageName;

        $category->save();

        return redirect()->route('category.index')->with('status', 'Успешна промена')->with('status_code', 'info');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        if (file_exists('uploads/category/' . $category->image)) {
            unlink('uploads/category/' . $category->image);
        }
        $category->delete();

        return redirect()->back()->with('status', 'Успешно бришење')->with('status_code', 'success');
    }
}
