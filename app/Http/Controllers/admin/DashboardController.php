<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Post;
use App\Slider;
use App\Transport;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $countSlider=Slider::all()->count();
        $countCategory=Category::all()->count();
        $countPost=Post::all()->count();
        $countTransport=Transport::all()->count();
        return view('admin.dashboard',compact('countSlider','countCategory','countPost','countTransport'));
    }
}
