<?php

namespace App\Http\Controllers\admin;

use App\Album;
use App\Http\Controllers\Controller;
use App\ImageAlbum;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ImageAlbumController extends Controller
{
    public function index()
    {
        $imagealbum=ImageAlbum::all();

        return view('admin.imagealbum.index',compact('imagealbum'));
    }

    public function create()
    {
        $album=Album::all();

        return view('admin.imagealbum.create',compact('album'));
    }

    public function store(Request $request)
    {
       /* $this->validate($request,[
            'album'=>'required',
            'image'=>'required|mimes:jpeg,jpg,bmp,png',
        ],[

            'image.required' => 'Немате внесено слика',
            'album.required' => 'Немате внесено албум'
        ]);*/
        foreach(request()->file('images') as $file ) {
            $imagealbum = new ImageAlbum();
            $filename = $file->getClientOriginalName();

            if (!file_exists('uploads/imagealbum')){
                mkdir('uploads/imagealbum', 0777, true);
            }
            $file->move('uploads/imagealbum', $filename);

            $imagealbum->album_id=$request->album;
            $imagealbum->image=$filename;

            $imagealbum->save();
        }
        return redirect()->route('imagealbum.index')->with('status', 'Успешно Внесување')->with('status_code', 'success');

    }

    public function destroy($id)
    {
        $imagealbum=ImageAlbum::findOrFail($id);
        if (file_exists('uploads/imagealbum/' . $imagealbum->image)) {
            unlink('uploads/imagealbum/' . $imagealbum->image);
        }
        $imagealbum->delete();

        return redirect()->back()->with('status', 'Успешно бришење')->with('status_code', 'success');
    }
}
