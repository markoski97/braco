<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\ImagePost;
use App\Post;
use Illuminate\Http\Request;

class ImagePostController extends Controller
{
    public function index()
    {
        $imagepost=ImagePost::all();

        return view('admin.imagepost.index',compact('imagepost'));
    }
    public function create()
    {
        $post=Post::all();

        return view('admin.imagepost.create',compact('post'));
    }

    public function store(Request $request)
    {
        foreach(request()->file('images') as $file ) {
            $imagepost = new ImagePost();
            $filename = $file->getClientOriginalName();

            if (!file_exists('uploads/imagepost')){
                mkdir('uploads/imagepost', 0777, true);
            }
            $file->move('uploads/imagepost', $filename);

            $imagepost->post_id=$request->post;
            $imagepost->image=$filename;

            $imagepost->save();
        }
        return redirect()->route('imagepost.index')->with('status', 'Успешно Внесување')->with('status_code', 'success');
    }

    public function destroy($id)
    {
        $imagepost=ImagePost::findOrFail($id);
        if (file_exists('uploads/imagepost/' . $imagepost->image)) {
            unlink('uploads/imagepost/' . $imagepost->image);
        }
        $imagepost->delete();

        return redirect()->back()->with('status', 'Успешно бришење')->with('status_code', 'success');
    }
}
