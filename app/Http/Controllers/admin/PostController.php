<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function index()
    {
        $post=Post::all();
        return view('admin.post.index',compact('post'));
    }

    public function create()
    {
        $category=Category::all();
        return view('admin.post.create',compact('category'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'category'=>'required',
            'title'=>'required',
            'description'=>'required',
            'image'=>'required|mimes:jpeg,jpg,bmp,png',
        ],[
            'title.required' => 'Немате внесено наслов',
            'description.required' => 'Немате внесено опис',
            'image.required' => 'Немате внесено слика',
            'category.required' => 'Немате внесено категорија'
        ]);
        $image=$request->file('image');

        $slug = Str::slug($request->title);

        if(isset($image)){
            $imagename=$slug.'.'.uniqid().'.'.$image->getClientOriginalExtension();
            if(!file_exists('uploads/post')){
                mkdir('uploads/post',0777,true);
            }
            $image->move('uploads/post',$imagename);
        }
        else
        {
            $imagename = 'default.png';
        }

        $post=new Post();

        $post->category_id=$request->category;
        $post->title=$request->title;
        $post->description=$request->description;
        $post->price=$request->price;
        $post->image=$imagename;
        $post->slug=$slug;

        $post->save();
        return redirect()->route('post.index')->with('status', 'Успешно Внесување')->with('status_code', 'success');
    }

    public function edit($id)
    {
        $post=Post::findOrFail($id);
        $category=Category::all();

        return view('admin.post.edit',compact('post','category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'category'=>'required',
            'title'=>'required',
            'description'=>'required',
        ],[
            'title.required' => 'Немате внесено наслов',
            'description.required' => 'Немате внесено опис',
            'category.required' => 'Немате внесено категорија'
        ]);
        $post=Post::findOrFail($id);

        $image=$request->file('image');

        $slug = Str::slug($request->title);


        if(isset($image)){
            $imagename=$slug.'.'.uniqid().'.'.$image->getClientOriginalExtension();
            if(!file_exists('uploads/post')){
                mkdir('uploads/post',0777,true);
            }
            unlink('uploads/post/'.$post->image);
            $image->move('uploads/post',$imagename);
        }
        else
        {
            $imagename = $post->image;
        }


        $post->category_id=$request->category;
        $post->title=$request->title;
        $post->description=$request->description;
        $post->price=$request->price;
        $post->image=$imagename;
        $post->slug=$slug;

        $post->save();

        return redirect()->route('post.index')->with('status', 'Успешно Внесување')->with('status_code', 'success');

    }


    public function destroy($id)
    {
        $post=Post::findOrFail($id);
        if (file_exists('uploads/post/' . $post->image)) {
            unlink('uploads/post/' . $post->image);
        }
        $post->delete();

        return redirect()->back()->with('status', 'Успешно бришење')->with('status_code', 'success');
    }
}
