<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Slider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::all();
        return view('admin.slider.index', compact('sliders'));
    }

    public function create()
    {
        return view('admin.slider.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'sub_title' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif',
        ],
            [
                'title.required' => 'Немате внесено наслов',
                'sub_title.required' => 'Немате внесено опис',
                'image.required' => 'Немате внесено слика'
            ]);

        $slug = Str::slug($request->title);

        $image = $request->file('image');

        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '-' . $image->getClientOriginalName();

            if (!file_exists('uploads/slider')) {
                mkdir('uploads/slider', 0777, true);
            }
            $image->move('uploads/slider', $imageName);
        } else {
            $imageName = 'default.png';
        }
        $slider = new Slider();
        $slider->title = $request->title;
        $slider->sub_title = $request->sub_title;
        $slider->info_button = $request->info_button;
        $slider->image = $imageName;

        $slider->save();
        /*  Session::flash('status_code','success');*/
        return redirect()->route('slider.index')->with('status', 'Успешно Внесување')->with('status_code', 'success');
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.slider.edit', compact('slider'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'sub_title' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg,gif',
        ],
            [
                'title.required' => 'Немате внесено наслов',
                'sub_title.required' => 'Немате внесено опис',
                'image.required' => 'Немате внесено слика'
            ]);
        $slider = Slider::find($id);
        $slug = Str::slug($request->title);

        $image = $request->file('image');
        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '-' . $image->getClientOriginalName();

            if (!file_exists('uploads/slider')) {
                mkdir('uploads/slider', 0777, true);
            }
            $image->move('uploads/slider', $imageName);
        } else {
            $imageName = $slider->image;//Ako ne klaj nova slika Zemija starata
        }

        $slider->title = $request->title;
        $slider->sub_title = $request->sub_title;
        $slider->info_button = $request->info_button;
        $slider->image = $imageName;

        $slider->save();

        return redirect()->route('slider.index')->with('status', 'Успешна промена')->with('status_code', 'info');
    }

    public function destroy($id)
    {
        $slider = Slider::find($id);
        if (file_exists('uploads/slider/' . $slider->image)) {
            unlink('uploads/slider/' . $slider->image);
        }
        $slider->delete();
        /*   return response()->json(['status'=>'Успешно Бришење']);*/
        return redirect()->back()->with('status', 'Успешно бришење')->with('status_code', 'success');

    }
}
