<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Transport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TransportController extends Controller
{
    public function index()
    {
        $transport = Transport::all();
        return view('admin.transport.index', compact('transport'));
    }


    public function create()
    {
        return view('admin.transport.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif',

        ],
            [
                'title.required' => 'Немате внесено наслов',
                'description.required' => 'Немате внесено Опис',
                'image.required' => 'Немате внесено слика'
            ]);
        $slug = Str::slug($request->title);

        $image = $request->file('image');
        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '-' . $image->getClientOriginalName();

            if (!file_exists('uploads/transport')) {
                mkdir('uploads/transport', 0777, true);
            }
            $image->move('uploads/transport', $imageName);
        } else {
            $imageName = 'default.png';
        }

        $transport=new Transport();

        $transport->title=$request->title;
        $transport->origin=$request->origin;
        $transport->destination=$request->destination;
        $transport->price=$request->price;
        $transport->description=$request->description;
        $transport->image=$imageName;
        $transport->slug=$slug;

        $transport->save();
        return redirect()->route('transport.index')->with('status', 'Успешно Внесување')->with('status_code', 'success');
    }


    public function edit($id)
    {
        $transport=Transport::findOrFail($id);
        return view('admin.transport.edit',compact('transport'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ],
            [
                'title.required' => 'Немате внесено наслов',
                'description.required' => 'Немате внесено Опис',
            ]);
        $transport=Transport::findOrFail($id);

        $image=$request->file('image');

        $slug = Str::slug($request->title);


        if(isset($image)){
            $imageName=$slug.'.'.uniqid().'.'.$image->getClientOriginalExtension();
            if(!file_exists('uploads/transport')){
                mkdir('uploads/transport',0777,true);
            }
            unlink('uploads/transport/'.$transport->image);
            $image->move('uploads/transport',$imageName);
        }
        else
        {
            $imageName = $transport->image;
        }
        $transport->title=$request->title;
        $transport->origin=$request->origin;
        $transport->destination=$request->destination;
        $transport->price=$request->price;
        $transport->description=$request->description;
        $transport->image=$imageName;
        $transport->slug=$slug;

        $transport->save();

        return redirect()->route('transport.index')->with('status', 'Успешна промена')->with('status_code', 'success');
    }

    public function destroy($id)
    {
        $transport=Transport::findOrFail($id);
        if (file_exists('uploads/transport/' . $transport->image)) {
            unlink('uploads/transport/' . $transport->image);
        }

        $transport->delete();

        return redirect()->back()->with('status', 'Успешнo бришење')->with('status_code', 'success');
    }
}
