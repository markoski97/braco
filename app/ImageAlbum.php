<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageAlbum extends Model
{
    protected $table = 'image_album';

    public function album()
    {
        return $this->belongsTo('App\Album');
    }
}
