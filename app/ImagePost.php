<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagePost extends Model
{
    protected $table = 'imagepost';

    public function post()
    {
        return $this->belongsTo('App\Post');
    }
}
