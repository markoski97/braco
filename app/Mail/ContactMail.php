<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;
    public $request;

    public function __construct(Request $request)
    {
        $this->request=$request;
        return [
        'imeprezime'=>'required',
        'email'=>'required|email',
        'number'=>'required',
        'message'=>'min:5',
        'subject'=>'min:3'
    ];

    }

    public function build()
    {
        return $this->view('front.mail.contact');
    }
}
