@extends('layouts.app')

@section('title','Dashboard')

@push('css')
@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">panorama_wide_angle</i>
                            </div>
                            <p class="card-category">Слајдери:</p>
                            <h3 class="card-title">
                                {{$countSlider}}
                            </h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">add_box</i>
                                <a href="javascript:;">Додај Нов Слајдер</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">content_copy</i>
                            </div>
                            <p class="card-category">Категории</p>
                            <h3 class="card-title">
                                {{$countCategory}}
                            </h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">add_box</i>
                                <a href="javascript:;">Додај Нова Категорија</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-danger card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">amp_stories</i>
                            </div>
                            <p class="card-category">Постови</p>
                            <h3 class="card-title">{{$countPost}}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">add_box</i>
                                <a href="javascript:;">Додај Нов Пост</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-info card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">drive_eta</i>
                            </div>
                            <p class="card-category">Превоз</p>
                            <h3 class="card-title">{{$countTransport}}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">add_box</i>
                                <a href="javascript:;">Додај Нов Превоз</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')
@endpush
