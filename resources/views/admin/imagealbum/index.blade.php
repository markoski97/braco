@extends('layouts.app')

@section('title','Slider')

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('imagealbum.create')}}" class="btn btn-info">Креирај Слики за албум</a>

                    @include('layouts.partials.msg')

                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Сите Слики</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="table" class="table table-striped table-bordered" style="width:100%">
                                    <thead class="text-primary">

                                    <th>ID</th>

                                    <th>Албум</th>

                                    <th>Слика</th>

                                    <th>Креирано на:</th>

                                    <th>Последна промена на:</th>

                                    <th>Акција:</th>
                                    </thead>
                                    <tbody>
                                    @foreach($imagealbum as $key=>$imagealbums)
                                        <tr>
                                            <input type="hidden" class="id" value="{{$imagealbums->id}}">
                                            <td>
                                                {{$key + 1}}
                                            </td>

                                            <td>
                                                {{$imagealbums->album->title}}
                                            </td>

                                            <td>
                                                <img class="img-responsive img-thumbnail" id="icon"
                                                     src="{{asset('uploads/imagealbum/'.$imagealbums->image)}}"
                                                     style="width: 100px;height: 100px" onclick="image(this)">
                                            </td>

                                            <td>
                                                {{$imagealbums->created_at}}
                                            </td>

                                            <td>
                                                {{$imagealbums->updated_at}}
                                            </td>
                                            <td>
                                                <div class="text-center">
                                                <button type="button" class="btn btn-danger btn-sm button_delete"
                                                        onclick="deleteTag({{$imagealbums->id}})">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                                <form id="delete-form-{{$imagealbums->id}}"
                                                      action="{{route('imagealbum.destroy',$imagealbums->id)}}"
                                                      style="display: none" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    {{--  <button type="button" class="btn btn-danger btn-xs button_delete">delete</button>--}}
                                                </form>
                                                </div>

                                                {{-- onclick="if(confirm('are you sure you want to delete this')){
                                                      event.preventDefault();
                                                      document.getElementById('delete-form-{{$slider->id}}').submit();

                                                      }else{
                                                      event.preventDefault()
                                                      }
                                                      ">--}}

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table').DataTable();
        });
    </script>


    <script>
        @if(session('status'))
        Swal.fire({
            type: '{{session('status_code')}}',
            title: '{{session('status')}}',
            confirmButtonText: 'ok'
        });
        @endif
    </script>
    <script>
        function image(img) {
            var src = img.src;
            Swal.fire({
                imageUrl: src,
            });
        }
    </script>
    <script>
        function deleteTag(id) {
            Swal.fire({
                title: 'Потврда за бришење?',
                text: "Доколку избришиш податоците не може да се вратат назад!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Избриши!',
                cancelButtonText: 'Назад!'
            }).then((result) => {
                if (result.value) {
                    event.preventDefault();
                    document.getElementById('delete-form-' + id).submit();
                    /*    Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )*/
                } else {
                    Swal.fire({
                            title: "Податоците не се избришани!",
                            type: "success"
                        }
                    )
                }
            })

        }
        </script>
    {{--  <script>
          $(document).ready(function () {
              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });

              $('.button_delete').click(function (e) {
                  e.preventDefault();
                  var delete_id = $(this).closest("tr").find('.delete_value').val();

                  Swal.fire({
                      title: 'Are you sure?',
                      text: "You won't be able to revert this!",
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Yes, delete it!'
                  })

                      .then((Resoult) => {
                          if (Resoult) {
                              var data = {
                                  "_token": $('input[name="_token"]').val(),
                                  "id": delete_id,
                              };
                              $.ajax({
                                  method: 'DELETE',
                                  url: '/admin/slider/' + delete_id,
                                  data: data,
                                  success: function (response) {
                                      swal(response.status, {
                                          type: "success",
                                      })
                                          .then((result) => {
                                              location.reload();
                                          })
                                  }

                              });
                          }
                          else {
                              swal(response.status, {
                                  text:"You did not delete",
                                  type: "danger",
                              });

                          }
                      });
              })
          })
      </script>--}}

@endpush

