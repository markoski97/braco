@extends('layouts.app')

@section('title','Slider')

@push('css')

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())

                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="this.parentElement.style.display='none'">
                                    <i class="material-icons">close</i>
                                </button>
                                <span>
                                         <b> Danger - </b>{{ $error }}
                                        </span>
                            </div>
                        @endforeach

                    @endif
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Додај нови слики</h4>
                        </div>
                        <div class="card-content">
                            <form method="POST" action="{{route('imagepost.store')}}" enctype="multipart/form-data">
                                @csrf

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Пост:</label>
                                            <select class="form-control " name="post">
                                                <option disabled selected>Одбери</option>
                                                @foreach($post as $posts)
                                                    <option value="{{$posts->id}}">{{$posts->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Слики</label>
                                        <input type="file" name="images[]" multiple>
                                    </div>
                                </div>
                                <a href="{{route('imagealbum.index')}}" class="btn btn-danger">Назад</a>
                                <button type="submit" class="btn btn-primary">Зачувај</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
