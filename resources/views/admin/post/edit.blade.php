@extends('layouts.app')

@section('title','Slider')

@push('css')
    <link href="https://cdn.quilljs.com/1.2.6/quill.snow.css" rel="stylesheet">
@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())

                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="this.parentElement.style.display='none'">
                                    <i class="material-icons">close</i>
                                </button>
                                <span>
                                         <b> Danger - </b>{{ $error }}
                                        </span>
                            </div>
                        @endforeach

                    @endif
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Промена на Пост</h4>
                        </div>
                        <div class="card-content">
                            <form method="POST" action="{{route('post.update',$post->id)}}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Категорија</label>
                                            <select class="form-control " name="category">
                                                <option disabled selected>Одбери</option>
                                                @foreach($category as $categories)
                                                    <option {{$categories->id == $post->category->id ? 'selected':' '}}  {{--OVA E ZA DA GO DAVA PRESELEKTIRANO VO SELECTO--}}
                                                            value="{{$categories->id}}">{{$categories->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Наслов</label>
                                            <input type="text" class="form-control" name="title" value="{{$post->category->title}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Цена</label>
                                            <input type="number" class="form-control" name="price" value="{{$post->price}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Опис</label>
                                            <textarea type="text" class="form-control" name="description">
                                                {{$post->description}}
                                            </textarea>
                                            

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Image</label>
                                        <input type="file" name="image">

                                    </div>
                                </div>
                                <a href="{{route('post.index')}}" class="btn btn-danger">Назад</a>
                                <button type="submit" class="btn btn-primary">Зачувај</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({
            selector:'textarea',
            height: 500,
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor forecolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | '
        });</script>--}}
@endpush
