@extends('layouts.app')

@section('title','Slider')

@push('css')

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())

                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="this.parentElement.style.display='none'">
                                    <i class="material-icons">close</i>
                                </button>
                                <span>
                                         <b> Danger - </b>{{ $error }}
                                        </span>
                            </div>
                        @endforeach

                    @endif
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Промена на Слајдер</h4>
                        </div>
                        <div class="card-content">
                            <form method="POST" action="{{route('slider.update',$slider->id)}}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Наслов</label>
                                            <input type="text" class="form-control" name="title" value="{{$slider->title}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Опис</label>
                                            <input type="text" class="form-control" name="sub_title" value="{{$slider->sub_title}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Копче</label>
                                            <input type="text" class="form-control" name="info_button" value="{{$slider->info_button}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Image</label>
                                        <input type="file" name="image">

                                    </div>
                                </div>
                                <a href="{{route('slider.index')}}" class="btn btn-danger">Назад</a>
                                <button type="submit" class="btn btn-primary">Зачувај</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
