@extends('layouts.app')

@section('title','Slider')

@push('css')

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())

                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="this.parentElement.style.display='none'">
                                    <i class="material-icons">close</i>
                                </button>
                                <span>
                                         <b> Danger - </b>{{ $error }}
                                        </span>
                            </div>
                        @endforeach

                    @endif
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Промена на Слајдер</h4>
                        </div>
                        <div class="card-content">
                            <form method="POST" action="{{route('transport.update',$transport->id)}}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Наслов</label>
                                            <input type="text" class="form-control" name="title" value="{{$transport->title}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Тргнување од</label>
                                            <input type="text" class="form-control" name="origin" value="{{$transport->origin}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Дестинација</label>
                                            <input type="text" class="form-control" name="destination" value="{{$transport->destination}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Цена</label>
                                            <input type="number" class="form-control" name="price" value="{{$transport->price}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Опис</label>
                                            <textarea type="text" class="form-control" name="description">
                                                {{$transport->description}}
                                            </textarea>


                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Image</label>
                                        <input type="file" name="image">

                                    </div>
                                </div>
                                <a href="{{route('slider.index')}}" class="btn btn-danger">Назад</a>
                                <button type="submit" class="btn btn-primary">Зачувај</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({
            selector:'textarea',
            height: 500,
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor forecolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | '
        });</script>--}}
@endpush
