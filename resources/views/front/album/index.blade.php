@extends('front.app')

@section('content')
    <!--  Page Content, class footer-fixed if footer is fixed  -->
    <div id="page-content" class="header-static footer-fixed">
        <!--  Slider  -->
        <div id="flexslider" class="fullpage-wrap small">
            <ul class="slides">
                <li style="background-image:url({{asset('front-end/images/dvojka.jpg')}})">
                    <div class="container text text-center">
                        <h1 class="white margin-bottom-small">Галерија</h1>
                    </div>
                    <div class="gradient dark"></div>
                </li>
                <ol class="breadcrumb">
                    <li><a href="{{url('/pocetna')}}">Почетна</a></li>
                    <li class="active">Галерија</li>
                </ol>
            </ul>
        </div>
        <!--  END Slider  -->
        <div id="page-wrap" class="content-section fullpage-wrap grey-background">
            <div class="container text">
                <!--  All treks  -->
                <section id="showcase-treks" class="page" data-isotope="load-simple">
                    <div class="masonry-items two-columns">
                        <!--  Single Trek  -->
                        @foreach($album as $albums)
                            <div class="item one-item trekking">
                                <div class="showcase-trek">

                                    <img src="{{asset('uploads/album/'.$albums->image)}}" style="height:300px" alt="">
                                    <div class="content text-center">
                                        <div class="row margin-leftright-null">
                                            <div class="category">
                                                <h3>{{$albums->title}}</h3>
                                            </div>
                                            <div class="info">
                                                <div class="col-md-12 padding-leftright-null">
                                                    <a href="/album/{{$albums->slug}}" class="btn-alt medium active margin-null">Повеќе</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="/album/{{$albums->slug}}" class="link"></a>
                                </div>
                            </div>
                            <!--  END Single Trek  -->
                        @endforeach
                    </div>
                </section>
                <!--  END All treks  -->
            </div>
            <!--  Call to Action  -->
        @include('front.layouts.contact_nadfuter')
        <!--  END Call to Action  -->
        </div>
    </div>
@endsection


