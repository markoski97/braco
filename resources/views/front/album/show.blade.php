@extends('front.app')

@section('content')
    <!--  Page Content, class footer-fixed if footer is fixed  -->
    <div id="page-content" class="header-static footer-fixed">
        <!--  Slider  -->
        <div id="flexslider" class="fullpage-wrap small">
            <ul class="slides">
                <li style="background-image:url({{asset('front-end/images/dvojka.jpg')}})">
                    <div class="container text text-center">
                        <h1 class="white margin-bottom-small">{{$album->title}}</h1>
                    </div>
                    <div class="gradient dark"></div>
                </li>
                <ol class="breadcrumb">
                    <li><a href="{{url('/pocetna')}}">Почетна</a></li>
                    <li class="active">Галерија</li>
                </ol>
            </ul>
        </div>
        <div id="page-wrap" class="content-section fullpage-wrap">
            <!-- Description Trek -->
            <div class="row margin-leftright-null">
                <div class="container">
                    <div class="col-md-12 text padding-bottom-null text-center">
                        <h2 class="margin-bottom-null title line center">{{$album->title}}</h2>
                    </div>
                    <div class="col-md-12 text-center">
                        {!!$album->description !!}
                    </div>
                </div>
            </div>
            <!-- END Description Trek -->


            <!--  Gallery Section  -->
            <section id="gallery" data-isotope="load-simple">
                <div class="masonry-items equal three-columns">
                    @foreach($albumimage as $albumimages)
                    <!--  Lightbox trek -->
                    <div class="one-item">
                        <div class="image-bg" style="background-image:url({{asset('uploads/imagealbum/'.$albumimages->image)}})"></div>
                        <div class="content figure">
                            <i class="pd-icon-camera"></i>
                            <a href="{{asset('uploads/imagealbum/'.$albumimages->image)}}" class="link lightbox"></a>
                        </div>
                    </div>

                    @endforeach
                    <!--  END Lightbox trek -->
                </div>
            </section>
            <!--  END Gallery Section  -->
        </div>
    </div>

    <!--  Call to Action  -->
    @include('front.layouts.contact_nadfuter')
    <!--  END Call to Action  -->
@endsection


