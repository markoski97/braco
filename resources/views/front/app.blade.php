<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Еxтравел</title>
    <link rel="shortcut icon" href="front-end/images/favicon.png">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap/bootstrap.min.css')}}">

    <!-- Optional theme -->
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap/bootstrap-theme.min.css')}}">

    <!-- Custom css -->
    <link rel="stylesheet" href="{{asset('front-end/css/style.css')}}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('front-end/css/font-awesome.min.css')}}">

    <link rel="stylesheet" href="{{asset('front-end/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('front-end/css/puredesign.css')}}">

    <!-- Flexslider -->
    <link rel="stylesheet" href="{{asset('front-end/css/flexslider.css')}}">

    <!-- Owl -->
    <link rel="stylesheet" href="{{asset('front-end/css/owl.carousel.css')}}">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{asset('front-end/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('front-end/css/jquery.fullPage.css')}}">

    @stack('css')
</head>

<body>

<!--  loader  -->
<div id="myloader">
        <span class="loader">
                <div class="inner-loader"></div>
            </span>
</div>

<!--  Main Wrap  -->
<div id="main-wrap" class="full-width">

   @include('front.layouts.header')

   @yield('content')

    @include('front.layouts.footer')
</div>
<!--  Main Wrap  -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('front-end/js/jquery.min.js')}}"></script>
<!-- All js library -->
<script src="{{asset('front-end/js/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('front-end/js/jquery.flexslider-min.js')}}"></script>
<script src="{{asset('front-end/js/jquery.fullPage.min.js')}}"></script>
<script src="{{asset('front-end/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('front-end/js/isotope.min.js')}}"></script>
<script src="{{asset('front-end/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('front-end/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{asset('front-end/js/smooth.scroll.min.js')}}"></script>
<script src="{{asset('front-end/js/jquery.appear.js')}}"></script>
<script src="{{asset('front-end/js/jquery.countTo.js')}}"></script>
<script src="{{asset('front-end/js/jquery.scrolly.js')}}"></script>
<script src="{{asset('front-end/js/plugins-scroll.js')}}"></script>
<script src="{{asset('front-end/js/imagesloaded.min.js')}}"></script>
<script src="{{asset('front-end/js/pace.min.js')}}"></script>
<script src="{{asset('front-end/js/main.js')}}"></script>
</body>

</html>
