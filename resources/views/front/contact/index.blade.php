@extends('front.app')

@section('content')
    <!--  Page Content, class footer-fixed if footer is fixed  -->
    <div id="page-content" class="header-static footer-fixed">
        <!--  Slider  -->
        <div id="flexslider" class="fullpage-wrap small">
            <ul class="slides">
                <li style="background-image:url({{asset('front-end/images/dvojka.jpg')}})">
                    <div class="container text text-center">
                        <h1 class="white margin-bottom-small">Контакт</h1>
                    </div>
                    <div class="gradient dark"></div>
                </li>
                <ol class="breadcrumb">
                    <li><a href="{{url('/pocetna')}}">Почетна</a></li>
                    <li class="active">Контакт</li>
                </ol>
            </ul>
        </div>

        <div id="page-wrap" class="content-section fullpage-wrap">
            <div class="row margin-leftright-null">
                <div class="container">
                    <!--  Contact Info  -->
                    <div class="col-md-6 padding-leftright-null">
                        <div class="text">
                            <h2 class="margin-bottom-null title line left">Контакт:</h2>
                            <p class="heading center grey margin-bottom-null">Почитувани корисници, за било какви
                                прашања и информации ве молиме посетете не или контактирајте не:</p>
                            <div class="padding-onlytop-md">
                                <p>
                                    <span class="contact-info">
                                        Адреса <em>"Борка Утот" Број 9а - Прилеп
                                        </em></span>
                                    <br>

                                    <span
                                        class="contact-info">
                                        Канцеларија
                                        <em>
                                            048 417 447
                                        </em>
                                    </span>

                                    <br>

                                    <span
                                        class="contact-info">
                                        Јордан
                                        <em>
                                            075 337 575
                                        </em>
                                    </span>
                                    <br>

                                    <span
                                        class="contact-info">
                                        Зоран
                                        <em>
                                            070 259 009
                                        </em>
                                    </span>
                                    <br>

                                    <span
                                        class="contact-info">
                                      Кире
                                        <em>
                                           078 353737
                                        </em>
                                    </span>
                                    <br>

                                    <span
                                        class="contact-info">
                                        Email:
                                        <br>
                                            <em>braco_prilep@yahoo.com</em>
                                            <br>
                                            <em>ex-trave@hotmail.com</em>
                                    </span>
                                    <br>
                                    <span
                                        class="fa fa-facebook">
                                        Facebook:
                                        <a href="https://www.facebook.com/pg/TravelAgencyBraco/about/?ref=page_internal">
                                            <em>ЕхТравел</em>
                                        </a>
                                    </span>
                                </p>

                                <p class="margin-md-bottom-null">
                                    <span class="contact-info">
                                        Понеделник - Петок
                                        <em>08:00 - 16:00 часот</em>
                                    </span>
                                    <br>

                                    <span
                                        class="contact-info">
                                        Сабота и Среда
                                        <em>
                                            10:00 - 15:00 часот
                                        </em>
                                    </span>
                                    <br>
                                    <span
                                        class="contact-info">
                                       Недела и Државни Празници
                                        <em>
                                            Неработен
                                        </em>
                                    </span>

                                </p>
                            </div>
                        </div>
                    </div>
                    <!--  END Contact Info -->
                    <!--  Input Form  -->
                    <div class="col-md-6 padding-leftright-null">
                        <div class="text padding-onlybottom-sm padding-md-top-null">
                            <form action="{{route('postcontact')}}" method="POST" id="contact-form" class="padding-onlytop-md padding-md-topbottom-null" >
                               {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <input class="form-field" name="imeprezime" id="imeprezime" type="text" placeholder="Име и Презиме"required>
                                    </div>
                                    <div class="col-md-12">
                                        <input class="form-field" name="email" id="email" type="text" placeholder="Емаил">
                                    </div>

                                    <div class="col-md-12">
                                        <input class="form-field" name="number" id="number" type="number" placeholder="Телефон"required>
                                    </div>

                                    <div class="col-md-12">
                                        <input class="form-field" name="subject" id="subject" type="text"
                                               placeholder="Прашање"required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea class="form-field" name="message" id="message" rows="6"
                                                  placeholder="Вашата порака"></textarea>
                                        <div class="submit-area padding-onlytop-sm">
                                           <button type="submit" class="btn btn-primary">Испрати</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--  END Input Form  -->
                </div>
            </div>
            <div class="row margin-leftright-null">
                <!--  Map. Settings in assets/js/maps.js  -->
                <div class="col-md-12 padding-leftright-null map">
                    <div class="mapouter">
                        <div class="gmap_canvas">
                            <iframe width="828" height="344" id="gmap_canvas"
                                    src="https://maps.google.com/maps?q=prilep%20police&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                            <a href="https://www.embedgooglemap.net">embedgooglemap.net</a></div>
                        <style>.mapouter {
                                position: relative;
                                text-align: right;
                                height: 344px;
                                width: 1080px;
                            }

                            .gmap_canvas {
                                overflow: hidden;
                                background: none !important;
                                height: 344px;
                                width: 828px;
                            }</style>
                    </div>
                </div>
                <!--  END Map  -->
            </div>
        </div>
    </div>

    <!--  END Page Content, class footer-fixed if footer is fixed  -->

@endsection
