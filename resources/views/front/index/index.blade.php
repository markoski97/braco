@extends('front.app')

@section('content')
    <div id="page-content" class="header-static footer-fixed">

        @include('front.layouts.slider')
        <div id="home-wrap" class="content-section fullpage-wrap">
            @include('front.layouts.about_us')

            @include('front.layouts.services')

            @include('front.layouts.background')

            @if($posts->count())
                @include('front.layouts.trip')
            @endif
        </div>
    </div>
    <!--  END Page Content, class footer-fixed if footer is fixed  -->
@endsection
