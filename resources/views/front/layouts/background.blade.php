<div class="row margin-leftright-null grey-background">
    <div class="bg-img overlay simple-parallax responsive" style="background-image:url({{asset('front-end/images/minibus.jpg')}});height: 250px">
        <div class="container">
            <!-- Testimonials -->
            <section class="testimonials-carousel-simple col-md-12 text padding-bottom-null">
                <div class="item padding-leftright-null">
                    <div class="padding-top-null padding-bottom-null">
                    </div>
                </div>
                <div class="item padding-leftright-null">
                    <div class="padding-top-null padding-bottom-null">
                    </div>
                </div>

            </section>
            <!-- END Testimonials -->
        </div>
    </div>
</div>
