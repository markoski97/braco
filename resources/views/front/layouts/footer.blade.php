<!--  Footer. Class fixed for fixed footer  -->
<footer class="fixed full-width">
    <div class="container">
        <div class="row no-margin">
            <div class="col-sm-4 col-md-2 padding-leftright-null">
                <h6 class="heading white margin-bottom-extrasmall">Ex Travel</h6>
                <ul class="sitemap">
                    <li>
                        <a href="{{url('/')}}" class="active-item">Почетна</a>
                    </li>
                    <li>
                        <a href="{{url('/arazmani')}}">Аранжмани</a>
                    </li>
                    <li>
                        <a href="{{url('/prevoz')}}">Превоз</a>
                    </li>
                    <li>
                        <a href="{{url('/uslovi')}}">Услови на патување</a>
                    </li>
                    <li>
                        <a href="{{url('/album')}}">Галерија</a>
                    </li>
                    <li>
                        <a href="{{url('/kontakt')}}">Контакт</a>
                    </li>

                </ul>
            </div>
            <div class="col-sm-4 col-md-2 padding-leftright-null padding-right-md-2">
                <h6 class="heading white margin-bottom-extrasmall">Телефони</h6>
                <ul class="useful-links">
                    <li>Канцеларија: <br>048 417 447</li>
                    <li>Јордан: 075 337 575</li>
                    <li>Зоран:  070 259 009</li>
                    <li>Кире:  078 353 737</li>
                </ul>
            </div>
            <div class="col-sm-4 col-md-4 padding-leftright-null">
                <h6 class="heading white margin-bottom-extrasmall">Контакт</h6>
                <ul class="info">
                    <li> Понеделник - Петок
                        <span class="white">
                            09:00 - 17:00 часот
                        </span>
                        <br>
                        Сабота и Среда
                        <span class="white">
                            09:00 - 15:00 часот
                        </span>
                        <br>
                        Недела и Државни Празници
                        <span class="white">
                            Неработен
                        </span>
                    </li>
                    <li>
                        Адреса: "Борка Утот" Број 9а - Прилеп
                    </li>

                    <li>
                        Email: braco_prilep@yahoo.com
                    </li>
                </ul>
            </div>
            <div class="col-md-4 padding-leftright-null">
                <a class="navbar-brand" href="{{url('/login')}}">
                    <img src="{{asset('front-end/images/ex_travel_logo.png')}}" class="normal" alt="logo" style="height: 170px">
                </a>
            </div>
        </div>
        <div class="copy">
            <div class="row ">
                <div class="col-md-12 text-center">
                    &copy; Агенција Ех Травел - 048 417 447
                </div>
            </div>
        </div>
    </div>
</footer>
<!--  END Footer. Class fixed for fixed footer  -->
