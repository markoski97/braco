<!--  Header & Menu  -->
<header id="header" class="full-width transparent">
    <div class="container">
        <nav class="navbar navbar-default ">
            <!--  Header Logo  -->
            <div id="logo">
                <a class="navbar-brand" href="{{url('/pocetna')}}">
                    <img src="{{asset('front-end/images/ex_travel_logo.png')}}" class="normal" alt="logo">
                   {{--<img src="assets/img/logo.png" class="normal" alt="logo">
                    <img src="assets/img/logo@2x.png" class="retina" alt="logo">--}}
                </a>
            </div>
            <!--  END Header Logo  -->
            <!--  Classic menu, responsive menu classic  -->
            <div id="menu-classic">
                <div class="menu-holder">
                    <ul>
                        <li class="submenu">
                            <a href="{{url('/')}}" class="{{Request::is('/')? 'active-item': ''}}">Почетна</a>
                        </li>
                        <li class="submenu">
                            <a href="{{url('/arazmani')}}" class="{{Request::is('arazmani')? 'active-item': ''}}">Аранжмани</a>
                        </li>

                        <li class="submenu">
                            <a href="{{url('/prevoz')}}" class="{{Request::is('prevoz')? 'active-item': ''}}">Превоз</a>
                        </li>

                        <li class="submenu">
                            <a href="{{url('/uslovi')}}" class="{{Request::is('uslovi')? 'active-item': ''}}">Услови на патување</a>
                        </li>
                        <li>
                            <a href="{{url('/album')}}" class="{{Request::is('album')? 'active-item': ''}}">Галерија</a>
                        </li>
                        <li>
                            <a href="{{url('/kontakt')}}" class="{{Request::is('kontakt')? 'active-item': ''}}">Контакт</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--  END Classic menu, responsive menu classic  -->
            <!--  Button for Responsive Menu Classic  -->
            <div id="menu-responsive-classic">
                <div class="menu-button">
                    <span class="bar bar-1"></span>
                    <span class="bar bar-2"></span>
                    <span class="bar bar-3"></span>
                </div>
            </div>
            <!--  END Button for Responsive Menu Classic  -->
        </nav>
    </div>
</header>
<!--  END Header & Menu  -->
