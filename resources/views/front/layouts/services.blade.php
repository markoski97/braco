<!-- Services -->
<div class="row no-margin text-center grey-background">
    <div class="container">

        <div class="col-md-12 padding-leftright-null">
            <div class="col-md-4 padding-leftright-null">
                <div class="text padding-md-bottom-null">
                    <i class="pd-icon-camp-bag service margin-bottom-null"></i>
                    <h6 class="heading margin-bottom-extrasmall">Професионална услуга</h6>
                    <p class="margin-bottom-null">
                        Со нашето искуство и кадровската професионалност, им служиме на нашите клиенти повеќе од 20 години.
                    </p>
                </div>
                </div>

            <div class="col-md-4 padding-leftright-null">
                <div class="text padding-md-bottom-null">
                    <i class="pd-icon-man-people-streamline-user service margin-bottom-null"></i>
                    <h6 class="heading margin-bottom-extrasmall">Постојана подршка</h6>
                    <p class="margin-bottom-null">
                        Одговорноста и грижата за своите клиенти, секојдневно ја докажуваме со постојаната подршка и достапност на нашите вработени.
                    </p>
                </div>
            </div>

            <div class="col-md-4 padding-leftright-null">
                <div class="text">
                    <i class="pd-icon-increasing-chart service margin-bottom-null"></i>
                    <h6 class="heading  margin-bottom-extrasmall">Поволни цени</h6>
                    <p class="margin-bottom-null">
                        Прифатливите цени, во реална корелација со понудените услуги и аранжмани, најдобро ги дефинира огромниот број на постојани корисници.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Services -->
