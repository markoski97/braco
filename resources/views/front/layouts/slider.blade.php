<!--  Slider  -->
<div id="flexslider-nav" class="fullpage-wrap small">
    <ul class="slides">
        @foreach($sliders as $slider)
            src="{{asset('uploads/slider/'.$slider->image)}}"
        <li style="background-image:url({{'uploads/slider/'.$slider->image}})">
            <div class="container text">
                <h1 class="white flex-animation"> {{$slider->title}}</h1>
                <h2 class="white flex-animation">{{$slider->sub_title}}</h2>
                @if($slider->info_button!=NULL)
                <a href="{{$slider->info_button}}" class="shadow btn-alt small activetwo margin-bottom-null flex-animation">Повеќе</a>
                    @endif
            </div>
            <div class="gradient dark"></div>
        </li>
        @endforeach
    </ul>
    <div class="slider-navigation">
        <a href="#" class="flex-prev"><i class="icon ion-ios-arrow-thin-left"></i></a>
        <div class="slider-controls-container"></div>
        <a href="#" class="flex-next"><i class="icon ion-ios-arrow-thin-right"></i></a>
    </div>
</div>
<!--  END Slider  -->
