<!-- Trip Showcase  -->
<div id="showcase-treks" class="text padding-bottom-null white-background center">
    <div class="container">
        <div class="col-md-12 padding-leftright-null text-center">
            <h2 class="margin-bottom-null title line center">Најнови Аранжмани</h2>

        </div>
        <div class="col-md-12 padding-leftright-null">
            <section class="showcase-carousel text">
                <!--  Single Trip  -->
                @foreach($posts as $post)
                <div class="item">
                    <div class="showcase-trek">

                        <img src="{{asset('uploads/post/'.$post->image)}}" alt="" style="min-height: 250px;max-height:251px">
                        <div class="content text-center">
                            <div class="row margin-leftright-null">
                                <div class="category">
                                    <h3>{{$post->title}}</h3>
                                </div>
                                <div class="info">
                                    <div class="col-md-12 padding-leftright-null">
                                        <h6 class="heading">{{$post->category->title}}</h6>
                                        <p class="margin-null">Повеќе Инфомации</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="/postoj/{{$post->slug}}" class="link"></a>
                    </div>
                </div>
                <!--  END Single Trip  -->
                @endforeach

            </section>
        </div>
    </div>
</div>

<!--  END Trip Showcase  -->
