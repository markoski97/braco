@extends('front.app')

@section('content')

    <!--  Page Content, class footer-fixed if footer is fixed  -->
    <div id="page-content" class="header-static footer-fixed">
        <!--  Slider  -->
        <div id="flexslider" class="fullpage-wrap small">
            <ul class="slides">
                <li style="background-image:url({{asset('front-end/images/dvojka.jpg')}})">
                    <div class="container text text-center">
                        <h1 class="white margin-bottom-small">{{$post->title}}</h1>
                        <p class="heading white">
                        {{--{{$category->description}}</p>--}}
                    </div>
                    <div class="gradient dark"></div>
                </li>
                <ol class="breadcrumb">
                    <li><a href="{{url('/pocetna')}}">Почетна</a></li>
                    <li class="active">{{$post->title}}</li>
                </ol>
            </ul>
        </div>
        <!--  END Slider  -->
        <div id="post-wrap" class="content-section fullpage-wrap">

            <!-- Description Trek -->
            <div class="row margin-leftright-null">
                <div class="container">
                    <div class="col-md-12 text padding-bottom-null text-center">
                        <h2 class="margin-bottom-null title line center">Повеќе информации</h2>
                    </div>
                    <div class="col-md-12 text">
                        {!!$post->description !!}
                    </div>
                </div>
            </div>
            <!-- END Description Trek -->

                <div class="row margin-leftright-null">
                    <div class="container text">
                        <!-- Simple Gallery -->
                        <section class="grid-images padding-top-null">
                            <div class="row">
                                @foreach($postimage as $postimages )
                                <div class="col-md-6">
                                    <div class="image simple-shadow" style="background-image:url({{asset('uploads/imagepost/'.$postimages->image)}})">
                                        <a class="lightbox-image" href="{{asset('uploads/imagepost/'.$postimages->image)}}"></a>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </section>
                        <!-- END Simple Gallery -->
                    </div>
                </div>
        </div>
    </div>



@endsection
