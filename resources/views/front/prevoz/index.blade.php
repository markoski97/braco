@extends('front.app')
@section('content')
    <!--  Page Content, class footer-fixed if footer is fixed  -->
    <div id="page-content" class="header-static footer-fixed">
        <!--  Slider  -->
        <div id="flexslider" class="fullpage-wrap small">
            <ul class="slides">
                <li style="background-image:url({{asset('front-end/images/dvojka.jpg')}})">
                    <div class="container text text-center">
                        <h1 class="white margin-bottom-small">Превоз</h1>
                        <p class="heading white">
                            АВТОБУСКИ ПРЕВОЗ ДО ДЕСТИНАЦИЈА
                            <br>
                            ИНФОРМАЦИИ, ЦЕНИ И РЕД НА ВОЗЕЊЕ
                        <p>
                    </div>
                    <div class="gradient dark"></div>
                </li>
                <ol class="breadcrumb">
                    <li><a href="{{url('/pocetna')}}">Почетна</a></li>
                    <li class="active">Превоз</li>
                </ol>
            </ul>
        </div>
        <!--  END Slider  -->
        <div id="page-wrap" class="content-section fullpage-wrap grey-background">
            <div class="container text">
                @if($transport->count())
                <section id="showcase-treks" class="page" data-isotope="load-simple">
                    <div class="masonry-items two-columns">
                        <!--  Single Trek  -->
                        @foreach($transport as $transports)
                            <div class="item one-item trekking">
                                <div class="showcase-trek">

                                    <img src="{{asset('uploads/transport/'.$transports->image)}}" style="height:300px" alt="">
                                    <div class="content text-center">
                                        <div class="row margin-leftright-null">
                                            <div class="category">
                                                <h3>{{$transports->title}}</h3>
                                            </div>
                                            <div class="info">
                                                <div class="col-md-12 padding-leftright-null">
                                                    <a href="/prevoz/{{$transports->slug}}" class="btn-alt medium active margin-null">Повеќе</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="/prevoz/{{$transports->slug}}"  class="link"></a>
                                </div>
                            </div>
                            <!--  END Single Trek  -->
                        @endforeach
                    </div>
                    @else
                        <div class="col-md-12">
                            <h1 class="text-center">Во Моментов Немаме Организирано Превоз.</h1>
                        </div>
                    @endif
                </section>
                <!--  END All treks  -->
            </div>
            <!--  Call to Action  -->
        @include('front.layouts.contact_nadfuter')
        <!--  END Call to Action  -->
        </div>
    </div>
@endsection
