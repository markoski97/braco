@extends('front.app')
@section('content')
    <div id="page-content" class="header-static footer-fixed">
        <!--  Slider  -->
        <div id="flexslider" class="fullpage-wrap small">
            <ul class="slides">
                <li style="background-image:url({{asset('front-end/images/minibus.jpg')}})">
                    <div class="container text text-center">
                        <h1 class="white margin-bottom-small">{{$transport->title}}</h1>
                        <p class="heading white">
                        {{--{{$category->description}}</p>--}}
                    </div>
                    <div class="gradient dark"></div>
                </li>
                <ol class="breadcrumb">
                    <li><a href="{{url('/pocetna')}}">Почетна</a></li>
                    <li class="active">{{$transport->title}}</li>
                </ol>
            </ul>
        </div>
        <!--  END Slider  -->
        <div id="post-wrap" class="content-section fullpage-wrap">

            <!-- Description Trek -->
            <div class="row margin-leftright-null">
                <div class="container">
                    <div class="col-md-12 text padding-bottom-null text-center">
                        <h2 class="margin-bottom-null title line center">Повеќе информации</h2>
                    </div>
                    <div class="col-md-12 text">
                        {!!$transport->description !!}
                    </div>
                </div>
            </div>
            <!-- END Description Trek -->
        </div>
    </div>

@endsection
