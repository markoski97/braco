<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"

          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="{{asset('back-end/css/material-dashboard.css?v=2.1.2')}}" rel="stylesheet"/>
    @stack('css')
</head>
<body>
<div class="wrapper">
    @if(Request::is('admin*'))
        @include('layouts.partials.sidebar')
    @endif

    <div class="main-panel">
        @if(Request::is('admin*'))
            @include('layouts.partials.topbar')
        @endif

        @yield('content')

        @if(Request::is('admin*'))
            @include('layouts.partials.footer')
        @endif

    </div>
</div>
</body>
<!--   Core JS Files   -->
<script src="{{asset('back-end/js/core/bootstrap-material-design.min.js')}}"></script>
<script src="{{asset('back-end/js/core/jquery.min.js')}}"></script>
<script src="{{asset('back-end/js/core/popper.min.js')}}"></script>

<script src="{{asset('back-end/js/plugins/arrive.min.js')}}"></script>
<script src="{{asset('back-end/js/plugins/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('back-end/js/plugins/bootstrap-notify.js')}}"></script>
<script src="{{asset('back-end/js/plugins/bootstrap-selectpicker.js')}}"></script>
<script src="{{asset('back-end/js/plugins/bootstrap-tagsinput.js')}}"></script>
<script src="{{asset('back-end/js/plugins/chartist.min.js')}}"></script>
<script src="{{asset('back-end/js/plugins/fullcalendar.min.js')}}"></script>
<script src="{{asset('back-end/js/plugins/jasny-bootstrap.min.js')}}"></script>
<script src="{{asset('back-end/js/plugins/jquery.bootstrap-wizard.js')}}"></script>
<script src="{{asset('back-end/js/plugins/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('back-end/js/plugins/jquery.tagsinput.js')}}"></script>
<script src="{{asset('back-end/js/plugins/jquery.validate.min.js')}}"></script>
<script src="{{asset('back-end/js/plugins/jquery-jvectormap.js')}}"></script>
<script src="{{asset('back-end/js/plugins/moment.min.js')}}"></script>
<script src="{{asset('back-end/js/plugins/nouislider.min.js')}}"></script>
<script src="{{asset('back-end/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{asset('back-end/js/plugins/sweetalert2.js')}}"></script>

<script src="{{asset('back-end/js/material-dashboard.js')}}"></script>
<script src="{{asset('back-end/js/material-dashboard.js.map')}}"></script>
<script src="{{asset('back-end/js/material-dashboard.min.js')}}"></script>

@stack('scripts')
</html>
