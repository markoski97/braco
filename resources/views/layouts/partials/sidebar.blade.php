<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{Request::is('admin/dashboard*')?'active':''}}">
                <a class="nav-link" href="{{route('admin.dashboard')}}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>

            <li class="{{Request::is('admin/slider*')?'active':''}}">
                <a class="nav-link" href="{{route('slider.index')}}">
                    <i class="material-icons">slideshow</i>
                    <p>Слајдери</p>
                </a>

            <li class="{{Request::is('admin/category*')?'active':''}}">
                <a class="nav-link" href="{{route('category.index')}}">
                    <i class="material-icons">category</i>
                    <p>Категории</p>
                </a>
            </li>


            <li class="{{Request::is('admin/post*')?'active':''}}">
                <a class="nav-link" href="{{route('post.index')}}">
                    <i class="material-icons">web_asset</i>
                    <p>Постови</p>
                </a>
            </li>

            <li class="{{Request::is('admin/imagepost*')?'active':''}}">
                <a class="nav-link" href="{{route('imagepost.index')}}">
                    <i class="material-icons">crop_original</i>
                    <p>Слики за Постови</p>
                </a>
            </li>

            <li class="{{Request::is('admin/transport*')?'active':''}}">
                <a class="nav-link" href="{{route('transport.index')}}">
                    <i class="material-icons">directions_bus</i>
                    <p>Транспорт</p>
                </a>
            </li>

            <li class="{{Request::is('admin/album*')?'active':''}}">
                <a class="nav-link" href="{{route('album.index')}}">
                    <i class="material-icons">camera_alt</i>
                    <p>Албум</p>
                </a>
            </li>

            <li class="{{Request::is('admin/imagealbum*')?'active':''}}">
                <a class="nav-link" href="{{route('imagealbum.index')}}">
                    <i class="material-icons">crop_original</i>
                    <p>Слики за Албум</p>
                </a>
            </li>

        </ul>
    </div>
</div>

