<?php

use Illuminate\Support\Facades\Route;

//Admin ruti

Auth::routes();

//Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'admin'], function () {

    Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');
    Route::resource('slider','SliderController');
    Route::resource('category','CategoryController');
    Route::resource('post','PostController');
    Route::resource('imagepost','ImagePostController');
    Route::resource('transport','TransportController');
    Route::resource('album','AlbumController');
    Route::resource('imagealbum','ImageAlbumController');

});
//Pocetna
Route::get('/', 'Front\FrontController@index');

//Aranzmani
Route::get('/arazmani', 'Front\FrontController@arazmani');
Route::get('/arazmani/{category}', 'Front\FrontController@show');

//Postoj
Route::get('/postoj/{post}', 'Front\FrontController@postojshow');

//prevoz
Route::get('/prevoz', 'Front\FrontController@prevozindex');
Route::get('/prevoz/{transport}', 'Front\FrontController@prevozshow');

//Uslovi
Route::get('/uslovi', 'Front\FrontController@uslovi');

//album
Route::get('/album', 'Front\FrontController@albumindex');
Route::get('/album/{album}', 'Front\FrontController@albumshow');

//contact
Route::get('/kontakt', 'Front\FrontController@contact');
Route::post('/kontakt', 'Front\FrontController@postcontact')->name('postcontact');


